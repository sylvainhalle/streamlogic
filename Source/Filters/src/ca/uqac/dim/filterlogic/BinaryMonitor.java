/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class BinaryMonitor.
 */
public abstract class BinaryMonitor extends TraceMonitor
{

	/** The m_left. */
	protected TraceMonitor m_left;

	/** The m_right. */
	protected TraceMonitor m_right;

	/**
	 * Instantiates a new binary monitor.
	 */
	public BinaryMonitor()
	{
		super();
	}

	/**
	 * Sets the left.
	 * 
	 * @param m
	 *            the new left
	 */
	public void setLeft(TraceMonitor m)
	{
		m_left = m;
	}

	/**
	 * Sets the right.
	 * 
	 * @param m
	 *            the new right
	 */
	public void setRight(TraceMonitor m)
	{
		m_right = m;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.uqac.dim.filterlogic.monitors.TraceMonitor#putFilteredMessage(ca.uqac
	 * .dim.filterlogic.Message)
	 */
	@Override
	public void putFilteredMessage(Message m)
	{
		Value left = m_left.evaluate();
		Value right = m_right.evaluate();
		if (left == Value.UNDEFINED)
		{
			m_left.putMessage(m);
		}
		if (right == Value.UNDEFINED)
		{
			m_right.putMessage(m);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return m_left.hashCode() + m_right.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof BinaryMonitor)
		{
			return equals((BinaryMonitor) o);
		}
		return false;
	}

	/**
	 * Equals.
	 * 
	 * @param o
	 *            the o
	 * @return true, if successful
	 */
	public boolean equals(BinaryMonitor o)
	{
		return m_left.equals(o.m_left) && m_right.equals(o.m_right);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#newCopy()
	 */
	@Override
	public abstract TraceMonitor newCopy();

}
