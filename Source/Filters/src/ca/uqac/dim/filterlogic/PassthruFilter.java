/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class PassthruFilter.
 */
public class PassthruFilter extends Filter
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.uqac.dim.filterlogic.Filter#putMessage(ca.uqac.dim.filterlogic.Message
	 * )
	 */
	@Override
	public void putMessage(Message m)
	{
		m_reader.putFilteredMessage(m);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.Filter#toString()
	 */
	@Override
	public String toString()
	{
		return "";
	}
}
