/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantMonitor.
 */
public class ConstantMonitor extends TraceMonitor
{

	/** The m_current state. */
	protected Value m_currentState = Value.UNDEFINED;

	/** The m_condition. */
	protected String m_condition = "";

	/**
	 * Instantiates a new constant monitor.
	 */
	public ConstantMonitor()
	{
		super();
	}

	/**
	 * Instantiates a new constant monitor.
	 * 
	 * @param s
	 *            the s
	 */
	public ConstantMonitor(String s)
	{
		super();
		setCondition(s);
	}

	/**
	 * Sets the condition.
	 * 
	 * @param s
	 *            the new condition
	 */
	public void setCondition(String s)
	{
		if (s != null)
		{
			m_condition = s;
		}
	}

	@Override
	public Value evaluate()
	{
		if (!m_messageSeen)
		{
			return TraceMonitor.Value.UNDEFINED;
		}
		return m_currentState;
	}

	@Override
	public void putFilteredMessage(Message m)
	{
		super.putFilteredMessage(m);
		if (! (m instanceof AtomicMessage))
			return; // Constant monitors are only good for atomic messages
		assert m instanceof AtomicMessage;
		AtomicMessage am = (AtomicMessage) m;
		if (am.getName().compareTo(m_condition) == 0)
		{
			m_currentState = Value.TRUE;
		} else
		{
			m_currentState = Value.FALSE;
		}
	}

	@Override
	public String toString()
	{
		return super.toString() + m_condition;
	}

	@Override
	public int hashCode()
	{
		return super.hashCode() + m_condition.hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof ConstantMonitor)
		{
			return equals((ConstantMonitor) o);
		}
		return false;
	}

	/**
	 * Equals.
	 * 
	 * @param c
	 *            the c
	 * @return true, if successful
	 */
	public boolean equals(ConstantMonitor c)
	{
		return m_condition.compareTo(c.m_condition) == 0;
	}

	@Override
	public TraceMonitor newCopy()
	{
		ConstantMonitor cm = new ConstantMonitor(m_condition);
		if (m_filter != null)
		{
			cm.setFilter(m_filter.newCopy());
		}
		return cm;
	}

}
