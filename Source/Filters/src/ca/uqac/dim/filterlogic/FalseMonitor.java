/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class FalseMonitor.
 */
public class FalseMonitor extends TraceMonitor
{
	
	public static final String SYMBOL = "0";

	/**
	 * Instantiates a new false monitor.
	 */
	public FalseMonitor()
	{
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#evaluate()
	 */
	@Override
	public TraceMonitor.Value evaluate()
	{
		if (!m_messageSeen)
		{
			return TraceMonitor.Value.UNDEFINED;
		}
		return TraceMonitor.Value.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.uqac.dim.filterlogic.monitors.TraceMonitor#putFilteredMessage(ca.uqac
	 * .dim.filterlogic.Message)
	 */
	@Override
	public void putFilteredMessage(Message m)
	{
		super.putFilteredMessage(m);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#toString()
	 */
	@Override
	public String toString()
	{
		return super.toString() + SYMBOL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return super.hashCode() + SYMBOL.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof FalseMonitor)
		{
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#newCopy()
	 */
	@Override
	public FalseMonitor newCopy()
	{
		FalseMonitor fm = new FalseMonitor();
		fm.setFilter(m_filter.newCopy());
		return fm;
	}
}
