/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

public class EqualsMonitor extends TraceMonitor
{
	public static final String SYMBOL = "=";
	
	protected Value m_currentState = Value.UNDEFINED;
	
	protected String m_xpathString;
	
	public EqualsMonitor()
	{
		super();
		m_xpathString = "";
	}
	
	public EqualsMonitor(String left, String right)
	{
		this();
		StringBuilder out = new StringBuilder();
		out.append(left).append(" = ").append(right);
		m_xpathString = out.toString();
	}

	@Override
	public TraceMonitor newCopy()
	{
		EqualsMonitor out = new EqualsMonitor();
		out.m_xpathString = new String(m_xpathString);
		out.m_currentState = m_currentState;
		return out;
	}
	
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder();
		out.append(super.toString());
		out.append(m_xpathString);
		return out.toString();
	}

	@Override
	public Value evaluate()
	{
		if (!m_messageSeen)
		{
			return TraceMonitor.Value.UNDEFINED;
		}
		return m_currentState;
	}
	
	@Override
	public void putFilteredMessage(Message m)
	{
		super.putFilteredMessage(m);
		if (!(m instanceof XMLMessage))
			return; // Should not happen, equality is only good for XML messages
		assert m instanceof XMLMessage;
		XMLMessage xm = (XMLMessage) m;
		boolean verdict = xm.evaluateXPath(m_xpathString);
		if (verdict == true)
			m_currentState = Value.TRUE;
		else
			m_currentState = Value.FALSE;
	}

}