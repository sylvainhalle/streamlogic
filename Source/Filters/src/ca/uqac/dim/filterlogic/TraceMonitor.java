/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

/**
 * Implementation of a monitor for message traces.
 * A monitor &phi; takes a trace m&#x305; = m<sub>1</sub>,
 * m<sub>2</sub>, &hellip; m<sub><i>n</i></sub> as an input, 
 * and returns as an output a sequence v&#x305; = v<sub>1</sub>, 
 * v<sub>2</sub>, &hellip;, where v<sub><i>i</i></sub> &isin; 
 * {&#x22a4;, &perp;}. This is noted m&#x305; : &phi;. Informally, a 
 * monitor is fed messages from m&#x305; one by one, and at any moment, 
 * can be queried for its current state, which by definition is the last 
 * symbol appended to v&#x305;. Hence a monitor queried multiple times 
 * without being fed any new message in between will return the same symbol. 
 * By definition, a monitor that has not been fed any message returns a 
 * third, special value noted "?".
 * @author Sylvain Hallé
 *
 */
public abstract class TraceMonitor implements MessageReader
{

	/**
	 * Possible outcomes for a monitor. True and false are self-explanatory;
	 * A monitor that has not yet been fed any message returns a third
	 * value called "UNDEFINED".
	 */
	public static enum Value {TRUE, FALSE, UNDEFINED};

	/**
	 * Evaluates the monitor in its current state.
	 * 
	 * @return One of the three possible outcomes.
	 */
	public abstract Value evaluate();

	/** The m_filter. */
	protected Filter m_filter;

	/** The m_message seen. */
	protected boolean m_messageSeen = false;

	/**
	 * Instantiates a new trace monitor.
	 */
	public TraceMonitor()
	{
		// m_filter = new PassthruFilter();
		// m_filter.setOutput(this);
	}

	public void putMessage(Message m)
	{
		if (m_filter != null)
		{
			m_filter.putMessage(m);
		} else
		{
			putFilteredMessage(m);
		}

	}

	public void putFilteredMessage(Message m)
	{
		m_messageSeen = true;
	}

	/**
	 * Sets the filter.
	 * 
	 * @param f
	 *            the new filter
	 */
	public void setFilter(Filter f)
	{
		m_filter = f;
		m_filter.setOutput(this);
	}

	@Override
	public String toString()
	{
		if (m_filter != null)
		{
			return m_filter.toString() + Filter.SYMBOL;
		}
		return "";
	}

	/**
	 * New copy.
	 * 
	 * @return the trace monitor
	 */
	public abstract TraceMonitor newCopy();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		if (m_filter != null)
		{
			return m_filter.hashCode();
		}
		return 0;
	}

}
