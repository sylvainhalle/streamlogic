/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

/**
 * Implementation of the negation monitor.
 */
public class NegationMonitor extends UnaryMonitor
{
	
	public static final String SYMBOL = "!";
	
	public NegationMonitor()
	{
		super();
	}

	/**
	 * Instantiates a new negation monitor.
	 * 
	 * @param m The TraceMonitor to take the negation of
	 */
	public NegationMonitor(TraceMonitor m)
	{
		super(m);
	}

	@Override
	public Value evaluate()
	{
		Value v = m_monitor.evaluate();
		if (v == Value.TRUE)
		{
			return Value.FALSE;
		}
		if (v == Value.FALSE)
		{
			return Value.TRUE;
		}
		return Value.UNDEFINED;
	}

	@Override
	public void putFilteredMessage(Message m)
	{
		m_monitor.putMessage(m);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder();
		out.append(super.toString()).append(SYMBOL).append(m_monitor.toString());
		return out.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.UnaryMonitor#newCopy()
	 */
	@Override
	public TraceMonitor newCopy()
	{
		NegationMonitor nm = new NegationMonitor(m_monitor.newCopy());
		if (m_filter != null)
		{
			nm.m_filter = m_filter.newCopy();
		}
		return nm;
	}

}
