/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class AndMonitor.
 */
public class FirstAndMonitor extends BinaryMonitor
{
	
	public static final String SYMBOL = "&1";

	/**
	 * Instantiates a new and monitor.
	 */
	public FirstAndMonitor()
	{
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#evaluate()
	 */
	@Override
	public Value evaluate()
	{
		Value left = m_left.evaluate();
		Value right = m_right.evaluate();
		if (left != Value.UNDEFINED)
		{
			if (right == Value.UNDEFINED)
				return left;
			else
			{
				if (left == Value.TRUE && right == Value.TRUE)
					return Value.TRUE;
				else
					return Value.FALSE;
			}
				
		}
		assert(left == Value.UNDEFINED);
		if (right != Value.UNDEFINED)
		{
			return right;				
		}
		return Value.UNDEFINED;
}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder out = new StringBuilder();
		out.append(super.toString());
		out.append(m_left.toString());
		out.append(" ").append(SYMBOL).append(" ");
		out.append(m_right.toString());
		return out.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.BinaryMonitor#newCopy()
	 */
	@Override
	public TraceMonitor newCopy()
	{
		FirstAndMonitor a = new FirstAndMonitor();
		a.setLeft(m_left.newCopy());
		a.setRight(m_right.newCopy());
		if (m_filter != null)
		{
			a.setFilter(m_filter.newCopy());
		}
		return a;
	}
}