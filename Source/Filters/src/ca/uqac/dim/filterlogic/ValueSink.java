/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

import java.util.*;

/**
 * Filter that simply records all messages sent to it. The ValueSink
 * is generally used to collect the results at the end of a chain of
 * filters.
 */
public class ValueSink extends Filter
{
	/** 
	 * Message queue
	 */
	Queue<Message> m_queue = new LinkedList<Message>();

	@Override
	public void putFilteredMessage(Message m)
	{
		m_queue.add(m);
	}

	@Override
	public String toString()
	{
		return m_queue.toString();
	}
}
