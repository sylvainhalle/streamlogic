/******************************************************************************
A simple LTL-FO+ trace validator
Copyright (C) 2009 Sylvain Halle

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 ******************************************************************************/

package ca.uqac.dim.filterlogic;

import java.io.*;

/**
 * The BeepBeepValidator is a simple trace validator using filter
 * expressions.
 * As a stand-alone command-line application, it can be invoked in the
 * following way:
 * <blockquote>
 * java -jar FilterValidator.jar &lt;tracefile&gt; &lt;propertyfile&gt;
 * </blockquote>
 * where <tt>tracefile</tt> designates a text file containing the sequence
 * of messages to work on, and <tt>propertyfile</tt> is a text file contaning
 * the filter expression to validate on that trace.
 * <p>
 * Some conventions apply to the trace file:
 * <ul>
 * <li>Messages must be formatted in XML</li>
 * <li>The top-level element must be called &lt;trace&gt;</li>
 * <li>Each message must be contained in a &lt;message&gt;...&lt;/message&gt; element</li>
 * <li>For other conventions regarding the format of XML messages, please see
 * {@link ca.uqam.info.xml#XMLDocument}</li>
 * </ul>
 * <p>
 * Some conventions apply to the property file:
 * <ul>
 * <li>The file can only contain one LTL-FO+ formula, formatted
 * following the rules of {@link ca.info.uqam.logic.LTLStringParser}</li>
 * <li>Any extra whitespace is ignored</li>
 * </ul>
 * In the background, the BeepBeepValidator uses exactly the same methods
 * as the {@link BeepBeepMonitor}.  The two differ only in their front-end.
 */
public class FilterValidator
{

	/**
	 * Gets the next message from an input stream. Crudely, this method keeps
	 * reading lines from the stream until it finds one that contains the string
	 * "&lt;/message&gt;". It ignores lines with elements called "&lt;trace&gt;"
	 * or "&lt;/trace&gt;". It returns null if it does not read anything.
	 * 
	 * @param dis
	 *          A data input stream, corresponding to some opened file
	 * @return A string corresponding to the next message, or null if no message
	 *         could be read
	 */
	@SuppressWarnings("deprecation")
	private static String getNextMessage(DataInputStream dis)
	{
		String message = "";
		try
		{
			while (dis.available() != 0)
			{
				String line = dis.readLine();
				line = line.trim();
				if (line.indexOf("<trace>") != -1 || line.indexOf("</trace>") != -1)
				{
					continue;
				}
				message += line;
				if (line.indexOf("</message>") != -1)
				{
					break;
				}
			}
		} catch ( IOException e)
		{
			return null;
		}
		message = message.trim();
		if (message.length() == 0)
		{
			return null;
		}
		return message;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{

		FileInputStream fis = null;
		BufferedInputStream bfis = null;
		DataInputStream dis = null;
		long heapSize = 0;

		// Prints credits
		System.err.println("Filter Trace Validator, version 0.1");
		System.err.println("(C) 2011-2013 Sylvain Hallé, Université du Québec à Chicoutimi");

		// Checks arguments
		if (args.length < 2)
		{
			System.err.println("ERROR: wrong command line parameters");
			System.err
			.println("Usage: java -jar FilterValidator.jar <trace-file> <property-file>");
			return;
		}

		// Parses arguments
		String inputFilename = args[0];
		String queryFilename = args[1];

		// Gets property to check
		String formula = readProperty(queryFilename);
		if (formula == null)
		{
			System.err.println("ERROR: cannot open " + queryFilename);
			return;
		}

		// Parses property to check
		TraceMonitor property = null;
		try
		{
			property = FilterParser.parseMonitorFromString(formula);
		}
		catch (FilterParser.ParseException ex)
		{
			System.err.println("ERROR: invalid property");
			System.err.println(ex.getMessage());
			System.exit(1);
		}
		assert property != null;

		// Opens file
		File traceFile = new File(inputFilename);
		try
		{
			fis = new FileInputStream(traceFile);
			bfis = new BufferedInputStream(fis);
			dis = new DataInputStream(bfis);
		} catch ( FileNotFoundException e)
		{
			System.err.println("ERROR: cannot open " + inputFilename);
			System.exit(1);
		}
		// A: here, dis points to a valid, opened trace file

		// Start stopwatch
		long timeBegin = System.nanoTime();

		// Start processing the trace
		long numMessages = 0;
		String message_content = getNextMessage(dis);
		Message message = null;
		while (message_content != null)
		{
			message = new XMLMessage(message_content);
			numMessages++;
			property.putMessage(message);
			message_content = getNextMessage(dis);
			//System.out.print(".");
			heapSize = Math.max(heapSize, Runtime.getRuntime().totalMemory());
		}

		// Stop stopwatch
		long timeEnd = System.nanoTime();

		// Processing is over, get stats...
		int milliseconds = (int) ((timeEnd - timeBegin) / (float) 1000000);
		String outcome = "INCONCLUSIVE";
		TraceMonitor.Value out = property.evaluate();
		if (out == TraceMonitor.Value.TRUE)
		{
			outcome = "TRUE";
		}
		if (out == TraceMonitor.Value.FALSE)
		{
			outcome = "FALSE";
		}
		System.err.println("Messages:  " + numMessages);
		System.err.println("Outcome:   " + outcome);
		System.err.println("Time (ms): " + milliseconds);
		System.err.println("Max heap:  " + heapSize);

		// ...close file
		try
		{
			if (dis != null)
			{
				dis.close();
			}
			if (bfis != null)
			{
				bfis.close();
			}
			if (fis != null)
			{
				fis.close();
			}
		} catch ( IOException e)
		{
			System.err.println("ERROR: while closing trace file");
			return;
		}

	}

	@SuppressWarnings("deprecation")
	private static String readProperty(String filename)
	{
		// Opens the file
		StringBuilder formula = new StringBuilder();
		File queryFile = new File(filename);
		try
		{
			FileInputStream fis = new FileInputStream(queryFile);
			BufferedInputStream bfis = new BufferedInputStream(fis);
			DataInputStream dis = new DataInputStream(bfis);
			while (dis.available() != 0)
			{
				formula.append(dis.readLine()).append("\n");
			}
			fis.close();
			bfis.close();
			dis.close();
		} catch ( FileNotFoundException e)
		{
			//e.printStackTrace();
			return null;
		} catch ( IOException e)
		{
			e.printStackTrace();
			return null;
		}
		return formula.toString();
	}

}