package ca.uqac.dim.filterlogic;

import java.util.List;

import ca.uqam.info.xml.XMLDocument;

public class XMLMessage extends Message
{
	protected XMLDocument m_document;

	public XMLMessage(String s)
	{
		super();
		m_document = new XMLDocument(s);
	}
	
	public boolean evaluateXPath(String s)
	{
		List<XMLDocument> answer = m_document.evaluateXPath(s);
		for (XMLDocument xd : answer)
		{
			if (xd.getType() == XMLDocument.NodeType.TRUE)
				return true;
			if (xd.getType() == XMLDocument.NodeType.FALSE)
				return false;
		}
		return false; // Should not happen
	}

}
