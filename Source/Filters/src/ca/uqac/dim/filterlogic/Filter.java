/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

import java.util.*;

import ca.uqac.dim.filterlogic.TraceMonitor.Value;

/**
 * The filter is an operator which, given some monitor &phi; and an input 
 * trace m&#x305;, returns a subtrace retaining only the symbols that match 
 * a specific condition. Formally, let m&#x305; = m<sub>1</sub>, 
 * m<sub>2</sub>, &hellip; be a message trace, andv&#x305; = v<sub>1</sub>, 
 * v<sub>2</sub>, &hellip; be the output sequence for monitor &phi; on that 
 * trace. The expression m&#x305; : &infin;/&phi; constructs from m&#x305; 
 * the subtrace made only of symbols m<sub><i>i</i></sub> such that 
 * v<sub><i>i</i></sub> = &#x22a4;. Hence, the filter &infin;/(c &lor; d) 
 * produces the subtrace made only of symbols c or d.
 * <p>
 * The &infin; symbol in the top part of the fraction indicates that one is to 
 * take all messages from m&#x305; that satisfy &phi;. We can also indicate 
 * to return only one particular message by replacing &infin; by a number. 
 * Hence k/&phi; returns only the k-th message that satisfies &phi; (i.e. 
 * m&#x305;'<sub>k</sub>).
 * @author Sylvain Hallé
 *
 */
public class Filter implements MessageReader
{
	
	public static final String SYMBOL = ":";
	public static final String FRAC_SYMBOL = "\\";

	/** The m_monitor. */
	TraceMonitor m_monitor;

	/** The m_reader. */
	MessageReader m_reader;

	/** The m_top number. */
	protected int m_topNumber = 1;

	/** The m_cur number. */
	protected int m_curNumber = 0;

	/** The m_queue. */
	protected LinkedList<MonTuple> m_queue = new LinkedList<MonTuple>();

	/** The INFINITY. */
	public static int INFINITY = 100;

	/**
	 * The Class MonTuple.
	 * 
	 * @author sylvain
	 */
	protected class MonTuple
	{

		/** The m_msg. */
		Message m_msg;

		/** The m_mon. */
		TraceMonitor m_mon;

		/**
		 * Instantiates a new tuple.
		 * 
		 * @param m
		 *            the m
		 * @param mon
		 *            the mon
		 */
		protected MonTuple(Message m, TraceMonitor mon)
		{
			m_msg = m;
			m_mon = mon;
		}

		@Override
		public int hashCode()
		{
			return m_msg.hashCode() + m_mon.hashCode();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object o)
		{
			if (o instanceof MonTuple)
			{
				return equals((MonTuple) o);
			}
			return false;
		}

		/**
		 * Equals.
		 * 
		 * @param mt
		 *            the mt
		 * @return true, if successful
		 */
		public boolean equals(MonTuple mt)
		{
			if (hashCode() != mt.hashCode())
			{
				return false;
			}
			if (!m_msg.equals(mt.m_msg))
			{
				return false;
			}
			if (!m_mon.equals(mt.m_mon))
			{
				return false;
			}
			return true;
		}
	}

	@Override
	public void putFilteredMessage(Message m)
	{
		MonTuple mt = new MonTuple(m, m_monitor.newCopy());
		m_queue.add(mt);
		Iterator<MonTuple> i = m_queue.iterator();
		while (i.hasNext())
		{
			MonTuple mont = i.next();
			TraceMonitor mon = mont.m_mon;
			mon.putMessage(m);
			Value v = mon.evaluate();
			if (v == Value.TRUE)
			{
				if (m_curNumber < m_topNumber || m_topNumber == INFINITY)
				{
					i.remove();
					m_reader.putFilteredMessage(mont.m_msg);
					m_curNumber++;
				}
			}
			if (v == Value.FALSE)
			{
				i.remove();
			}
		}
	}

	/**
	 * Sets the condition.
	 * 
	 * @param m
	 *            the new condition
	 */
	public void setCondition(TraceMonitor m)
	{
		m_monitor = m;
	}

	/**
	 * Sets the output.
	 * 
	 * @param r
	 *            the new output
	 */
	public void setOutput(MessageReader r)
	{
		m_reader = r;
	}

	/**
	 * Sets the top number.
	 * 
	 * @param n
	 *            the new top number
	 */
	public void setTopNumber(int n)
	{
		m_topNumber = n;
	}

	@Override
	public void putMessage(Message m)
	{
		putFilteredMessage(m);

	}

	@Override
	public String toString()
	{
		return m_topNumber + FRAC_SYMBOL + "(" + m_monitor.toString() + ")";
	}

	@Override
	public int hashCode()
	{
		return m_topNumber + m_monitor.hashCode() + m_reader.hashCode();
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Filter)
		{
			return equals((Filter) o);
		}
		return false;
	}

	public boolean equals(Filter o)
	{
		if (m_topNumber != o.m_topNumber)
		{
			return false;
		}
		if (m_curNumber != o.m_curNumber)
		{
			return false;
		}
		if (!m_monitor.equals(o.m_monitor))
		{
			return false;
		}
		return true;
	}

	/**
	 * Creates a deep copy of a filter.
	 * 
	 * @return A deep copy of the filter 
	 */
	public Filter newCopy()
	{
		Filter f = new Filter();
		f.m_monitor = m_monitor.newCopy();
		f.m_topNumber = m_topNumber;
		return f;
	}

}
