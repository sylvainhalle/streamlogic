/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

// TODO: Auto-generated Javadoc
/**
 * The Class UnaryMonitor.
 * 
 * @author sylvain
 */
public abstract class UnaryMonitor extends TraceMonitor
{

	/** The m_monitor. */
	protected TraceMonitor m_monitor;
	
	public UnaryMonitor()
	{
		super();
	}

	/**
	 * Instantiates a new unary monitor.
	 * 
	 * @param m
	 *            the m
	 */
	public UnaryMonitor(TraceMonitor m)
	{
		this();
		m_monitor = m;
	}
	
	public void setMonitor(TraceMonitor m)
	{
		m_monitor = m;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return m_monitor.hashCode() + 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof UnaryMonitor)
		{
			return equals((UnaryMonitor) o);
		}
		return false;
	}

	/**
	 * Equals.
	 * 
	 * @param o
	 *            the o
	 * @return true, if successful
	 */
	public boolean equals(UnaryMonitor o)
	{
		return m_monitor.equals(o.m_monitor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uqac.dim.filterlogic.monitors.TraceMonitor#newCopy()
	 */
	@Override
	public abstract TraceMonitor newCopy();

}
