/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.dim.filterlogic;

/**
 * Parse a string to build a chain of filters
 * @author Sylvain Hallé
 *
 */
public class FilterParser
{
	protected static String m_commentChar = "#";
	
	public static Filter parseFilterFromString(String s) throws ParseException
	{
		s = trimComments(s);
		return parseFilterFromString(s, null);
	}
	
	public static TraceMonitor parseMonitorFromString(String s) throws ParseException
	{
		s = trimComments(s);
		return parseMonitorFromString(s, null);
	}
	
	protected static TraceMonitor parseMonitorFromString(String s, Object bound_variables) throws ParseException
	{
		s = s.trim();
		if (s.isEmpty())
			throw new ParseException("Must parse an empty string");
		String c = s.substring(0, 1);
		TraceMonitor out = null;
		if (isQuantifierStart(c))
		{
			// Do nothing at the moment: quantifiers not supported
			throw new ParseException("Quantifiers are not supported");
		}
		else if (isUnaryOperator(c))
		{
			// Unary operator
			s = s.substring(1).trim();
			s = trimSurroundingPars(s);
			TraceMonitor in = parseMonitorFromString(s, bound_variables);
			UnaryMonitor uo = null;
			if (c.compareTo(NegationMonitor.SYMBOL) == 0)
				uo = new NegationMonitor();
			if (uo == null)
				throw new ParseException("Unrecognized operator \"" + c + "\" in \"" + s + "\"");
			uo.setMonitor(in);
			out = uo;
		}
		else if (containsBinaryOperator(s))
		{
			// Here, we know s contains either a binary operator or is
			// an atom. We discriminate by checking for the presence of
			// a binary operator
			String left = getLeft(s).trim();
			String right = getRight(s).trim();
			if (left.isEmpty() || right.isEmpty())
				throw new ParseException("Empty operand for binary operator in \"" + s + "\"");
			assert !left.isEmpty() && !right.isEmpty();
			int pars_left = s.indexOf(left);
			int pars_right = s.length() - s.lastIndexOf(right) - right.length();
			if (!(pars_left >= 0 && pars_right >= 0))
				throw new ParseException("Malformed expression \"" + s + "\"");
			assert pars_left >= 0 && pars_right >= 0;
			String op = getOperator(s, left.length() + pars_left * 2, right.length() + pars_right * 2);
			if (op.compareTo(Filter.SYMBOL) == 0)
			{
				Filter in_filter = parseFilterFromString(left, bound_variables);
				out = parseMonitorFromString(right, bound_variables);
				out.setFilter(in_filter);
			}
			else if (op.compareTo(Filter.FRAC_SYMBOL) == 0)
			{
				// We are parsing a filter containing a monitor, not a monitor
				throw new ParseException("Monitor expected, got a filter in \"" + s + "\"");
			}
			else if (op.compareTo(EqualsMonitor.SYMBOL) == 0)
			{
				// We are parsing an equality
				EqualsMonitor em = new EqualsMonitor(left, right);
				out = em;
			}
			else
			{
				TraceMonitor o_left = parseMonitorFromString(left, bound_variables);
				TraceMonitor o_right = parseMonitorFromString(right, bound_variables);
				if (o_left == null || o_right == null)
					throw new ParseException();
				BinaryMonitor bo = null;
				if (op.compareTo(AndMonitor.SYMBOL) == 0)
					bo = new AndMonitor();
				else if (op.compareTo(OrMonitor.SYMBOL) == 0)
					bo = new OrMonitor();
				else if (op.compareTo(FirstAndMonitor.SYMBOL) == 0)
					bo = new FirstAndMonitor();
				if (bo == null)
					throw new ParseException("Unrecognized binary operator \"" + op + "\" in \"" + s + "\"");
				bo.setLeft(o_left);
				bo.setRight(o_right);
				out = bo;
			}
		}
		else
		{
			// Constant, last remaining case
			if (s.compareTo(FalseMonitor.SYMBOL) == 0)
				out = new FalseMonitor();
			else if (s.compareTo(TrueMonitor.SYMBOL) == 0)
				out = new TrueMonitor();
			else
			{
				out = new ConstantMonitor(s);
			}
		}
		return out;
	}
	
	protected static Filter parseFilterFromString(String s, Object bound_variables) throws ParseException
	{
		Filter out = new Filter();
		s = s.trim();
		if (s.isEmpty())
			throw new ParseException("Must parse an empty string");
		String left = getLeft(s).trim();
		String right = getRight(s).trim();
		assert !left.isEmpty() && !right.isEmpty();
		int pars_left = s.indexOf(left);
		int pars_right = s.length() - s.lastIndexOf(right) - right.length();
		assert pars_left >= 0 && pars_right >= 0;
		String op = getOperator(s, left.length() + pars_left * 2, right.length() + pars_right * 2);
		if (op.compareTo(Filter.FRAC_SYMBOL) != 0)
		{
			// We are parsing a monitor containing a filter, not a filter
			throw new ParseException("Filter expected, got a monitor in \"" + s + "\"");
		}
		int top_number = 0;
		try
		{
			top_number = Integer.parseInt(left);
		}
		catch (NumberFormatException e)
		{
			throw new ParseException("Incorrect number format " + e.getMessage());
		}
		TraceMonitor mright = parseMonitorFromString(right, bound_variables);
		out.setTopNumber(top_number);
		out.setCondition(mright);
		return out;

	}
	
	private static String trimComments(String s)
	{
	  StringBuilder out = new StringBuilder();
      java.util.Scanner scanner = new java.util.Scanner(new java.io.ByteArrayInputStream(s.getBytes()));
      while (scanner.hasNextLine())
      {
        String line = scanner.nextLine();
        line = line.trim();
        if (line.isEmpty() || line.startsWith(m_commentChar))
          continue;
        out.append(line).append(" ");
      }
      return out.toString();
	}
	
	private static String trimSurroundingPars(String s)
	{
		if (s.startsWith(("(")))
		{
			// We trim surrounding parentheses, if any
			s = s.substring(1, s.length() - 1).trim();
		}
		return s;
	}
	
	private static boolean isQuantifierStart(String c)
	{
		//return c.compareTo(Exists.SYMBOL) == 0 || c.compareTo(ForAll.SYMBOL) == 0;
		return false; // No quantifiers at the moment
	}
	
	private static boolean isUnaryOperator(String c)
	{
		return c.compareTo(NegationMonitor.SYMBOL) == 0;
	}
	
	private static boolean containsBinaryOperator(String s)
	{
		return s.indexOf(AndMonitor.SYMBOL) != -1 
				|| s.indexOf(OrMonitor.SYMBOL) != -1
				|| s.indexOf(EqualsMonitor.SYMBOL) != -1 
				|| s.indexOf(Filter.SYMBOL) != -1
		        || s.indexOf(Filter.FRAC_SYMBOL) != -1;
	}
	
	private static String getLeft(String s)
	{
		if (s.startsWith("("))
		{
			// Find matching right parenthesis
			int paren_level = 1; 
			for (int i = 1; i < s.length(); i++)
			{
				String c = s.substring(i, i+1);
				if (c.compareTo("(") == 0)
					paren_level++;
				if (c.compareTo(")") == 0)
					paren_level--;
				if (paren_level == 0)
					return s.substring(1, i);
			}
		}
		else
		{
			// Loop until operator or space found
			for (int i = 1; i < s.length(); i++)
			{
				String c = s.substring(i, i+1);
				if (c.compareTo("(") == 0 || c.compareTo(")") == 0 || 
						c.compareTo(AndMonitor.SYMBOL) == 0  || c.compareTo(OrMonitor.SYMBOL) == 0 || 
						c.compareTo(Filter.FRAC_SYMBOL) == 0 || c.compareTo(Filter.SYMBOL) == 0 ||
						c.compareTo(EqualsMonitor.SYMBOL) == 0)
					return s.substring(0, i);
			}
		}
		return "";
	}
	
	private static String getRight(String s)
	{
		if (s.endsWith(")"))
		{
			// Find matching left parenthesis
			int paren_level = 1; 
			for (int i = s.length() - 1; i >= 0; i--)
			{
				String c = s.substring(i, i+1);
				if (c.compareTo(")") == 0)
					paren_level++;
				if (c.compareTo("(") == 0)
					paren_level--;
				if (paren_level == 1)
					return s.substring(i + 1, s.length() - 1);
			}
		}
		else
		{
			// Loop until operator or space found
			for (int i = s.length() - 1; i >= 0; i--)
			{
				String c = s.substring(i, i+1);
				if (c.compareTo("(") == 0 || c.compareTo(")") == 0 || 
						c.compareTo(AndMonitor.SYMBOL) == 0  || c.compareTo(OrMonitor.SYMBOL) == 0 || 
						c.compareTo(Filter.FRAC_SYMBOL) == 0 || c.compareTo(Filter.SYMBOL) == 0 ||
						c.compareTo(EqualsMonitor.SYMBOL) == 0)
					return s.substring(i + 1);
			}
		}
		return "";
	}
	
	private static String getOperator(String s, int size_left, int size_right)
	{
		assert size_left + size_right < s.length();
		return s.substring(size_left, s.length() - size_right).trim();
	}
	
	public static class ParseException extends Exception
	{
		/**
		 * Default serial ID
		 */
		private static final long serialVersionUID = 1L;
		
		private String m_message = "";
		
		public ParseException()
		{
			super();
		}
		
		public ParseException(String message)
		{
			this();
			m_message = message;
		}
		
		@Override
		public String getMessage()
		{
			return m_message;
		}
		
	}
}
