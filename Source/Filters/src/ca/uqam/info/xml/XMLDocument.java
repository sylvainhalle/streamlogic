/******************************************************************************
XML manipulation classes
Copyright (C) 2008 Sylvain Halle

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
******************************************************************************/
package ca.uqam.info.xml;

import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Iterator;

/**
 * Implementation of basic XPath functionalities for XML message manipulation.
 * An XMLDocument can be of four types:
 * <ol>
 * <li>ROOT: the root of the document</li>
 * <li>NODE: intermediate node in the document; it has a name, and possibly a
 * list of children</li>
 * <li>TEXT: leaf node containing a string of text</li>
 * <li>TRUE, FALSE: return type for boolean queries</li>
 * </ol>
 * An XMLDocument object can be created from a string using {@link
 * XMLDocument(string)}. The document is assumed to have a "normal structure",
 * i.e. no <tt>&lt;!CDATA&gt;</tt> sections or things of that kind. Attributes
 * inside elements (such as <tt>&lt;a attr="value"&gt;</tt>) do not cause
 * parsing errors, but they are ignored. Self-closing tags (e.g.
 * <tt>&lt;a/&gt;</tt>) are supported.
 * <p>
 * An XMLDocument can then be queried using a bare-bones implementation of
 * XPath. Only a tiny fragment of XPath is supported. Expressions can have three
 * forms:
 * <dl>
 * <dt><tt>p/p/p/.../p</tt></dt>
 * <dd>Returns a list of subtrees at the end of the path</dd>
 * <dt><tt>p/p/p/.../p = "exp"</tt></dt>
 * <dd>Returns a node with type TRUE if there is exactly one text node at the
 * end of the path with value "exp". Returns a node with type FALSE otherwise.</dd>
 * <dt><tt>p/p/p/.../p = p/p/p/.../p</tt></dt>
 * <dd>Same as above, but comparing two leafs in the tree.</dd>
 * </dl>
 * Please note that equality testings always operate on text leafs, and not
 * subtrees. Trying to compare two subtrees, or a subtree and a text value,
 * always returns false. This is different from standard XPath, and means that
 * you should not use expressions like that here! It returns false only so that
 * it returns something.
 * <p>
 * Moreover, comparison between elements is by default <strong>insensitive</strong>
 * to case.  This can be changed by instantiating XMLDocument with the
 * <tt>true</tt> argument. 
 * <p>
 * This class is kept as simple as possible <strong>on purpose</strong>. It is
 * not expected (nor desirable) that other functionalities be supported.
 * 
 * @author Sylvain Hall&eacute;
 * @version 2010-10-24
 * 
 */
public class XMLDocument {
	public static enum NodeType {
		NODE, TEXT, ROOT, TRUE, FALSE
	}

	protected static List<XMLDocument> parse(String s, boolean ignoreWhitespace) {
		Pattern pattern;
		Matcher matcher;
		int inside_begin = 0, inside_end = 0, tag_end = 0;
		boolean tag_has_children, parse_error = false;
		XMLDocument xd;
		List<XMLDocument> out_children = new Vector<XMLDocument>();
		
    if (s.length() >= 5 && s.substring(0, 5).compareToIgnoreCase("<?xml") == 0)
    {
      // This is an "XML declaration" tag: we trim it
      int new_beg = s.indexOf(">") + 1;
      s = s.substring(new_beg).trim();
    }
		while (s.length() > 0 && !parse_error) {
			xd = new XMLDocument();
			inside_begin = 0;
			inside_end = 0;
			tag_end = 0;
			tag_has_children = true;
			pattern = Pattern.compile("^<\\s*([^\\s>]+)\\s*([^>]*)>",
					Pattern.DOTALL);
			matcher = pattern.matcher(s);
			if (matcher.find()) {
				// Opening tag found at first position
				xd.m_elementName = matcher.group(1);
				inside_begin = matcher.end();
				if (matcher.group(2).endsWith("/")) {
					// Self-closing tag
					inside_end = matcher.end();
					tag_end = matcher.end();
					tag_has_children = false;
				} else {
					pattern = Pattern.compile("<\\s*(/{0,1})"
							+ xd.m_elementName + "\\s*>", Pattern.DOTALL);
					matcher = pattern.matcher(s.substring(inside_begin));
					int level = 1;
					boolean tag_found = false;
					while (matcher.find()) {
						if (matcher.group(1).startsWith("/")) {
							level--; // Closing tag
						}
						if (matcher.group(1).length() == 0) {
							level++; // Opening tag
						}
						if (level == 0) {
							// Matching closing tag
							inside_end = inside_begin + matcher.start();
							tag_end = inside_begin + matcher.end();
							tag_found = true;
							break;
						}
					}
					// If we get here, we didn't find matching closing tag:
					// parse error
					if (!tag_found) {
						parse_error = true; // Do nothing with it
					}
				}
			} else {
				// No opening tag at first position: this is text
				pattern = Pattern.compile("<", Pattern.DOTALL);
				matcher = pattern.matcher(s);
				if (matcher.find()) {
					inside_end = matcher.start();
				} else {
					inside_end = s.length();
				}
				tag_end = inside_end;
				String inside = s.substring(inside_begin, inside_end);
				if (ignoreWhitespace) {
					inside = inside.trim();
				}
				if (inside.length() == 0) {
					// This text is null, don't create an element
					xd = null;
				} else {
					xd.m_elementName = inside;
					xd.m_type = NodeType.TEXT;
					tag_has_children = false;
				}
			}
			if (!parse_error) {
				 String inside = s.substring(inside_begin, inside_end);
				s = s.substring(tag_end);
				if (xd != null) {
					if (tag_has_children) {
						xd.m_children = parse(inside, ignoreWhitespace);
					}
					out_children.add(xd);
				}
			}
		}
		return out_children;
	}

	protected List<XMLDocument> m_children = new Vector<XMLDocument>();

	protected boolean m_caseSensitive = false;
	
	protected String m_elementName = "";

	protected NodeType m_type = NodeType.NODE;

	public XMLDocument() {
		super();
	}
	
	public XMLDocument(boolean caseSensitive)
	{
	  m_caseSensitive = caseSensitive;
	}

	/**
	 * Delegate to {@link XMLDocument(String, boolean)} which ignores whitespace
	 * by default.
	 * 
	 * @param s
	 */
	public XMLDocument(String s) {
		this(s, true);
	}

	/**
	 * Creates an XML document from a string.
	 * 
	 * @param s
	 *            The XML code from which to create the document
	 * @param ignoreWhitespace
	 *            Optional. If set to false, ignores all intra-element
	 *            whitespace. Defaults to true (recommended, unless whitespace
	 *            preservation is important).
	 */
	public XMLDocument(String s, boolean ignoreWhitespace) {
		this();
		m_children = parse(s, ignoreWhitespace);
		m_type = NodeType.ROOT;
	}
	
	/**
	 * Creates a path in the document with specific value at the end
	 * @param path A slash-separated path
	 * @param value The value to put at the end of the path
	 */
	public void createPath(String path, String value)
	{
	  // Set myself as root
	  m_type = NodeType.ROOT;
	  // Splits the path
	  if (path == null || value == null)
	    return;
	  String[] elements = path.split("/");
	  int start = 0;
	  if (path.startsWith("/"))
	    start = 1;
	  Vector<String> splitPath = new Vector<String>();
	  for (int i = start; i < elements.length; i++)
	    splitPath.add(elements[i]);
	  createPath(splitPath, value);
	}
	
	private void createPath(Vector<String> path, String value)
	{
	  if (path.isEmpty())
	  {
	    // We are at the leaf; we create this leaf
	    XMLDocument leaf = new XMLDocument();
	    leaf.m_type = NodeType.TEXT;
	    leaf.m_elementName = value;
	    this.m_children.add(leaf);
	    return;
	  }
	  String firstElement = path.elementAt(0);
	  path.removeElementAt(0);
	  boolean added = false;
	  for (XMLDocument xd : m_children)
	  {
	    if (xd.m_elementName.compareTo(firstElement) != 0)
	      continue;
	    xd.createPath(path, value);
	    added = true;
	    break;
	  }
	  if (!added)
	  {
	    XMLDocument xd = new XMLDocument();
	    xd.m_elementName = firstElement;
	    xd.createPath(path, value);
	    m_children.add(xd);
	  }
	}

	/**
	 * Evaluates an XPath expression on a document. See the class documentation
	 * for a description of the supported syntax. In case of a syntax error, the
	 * expression evaluates to false as soon as the method's (very) basic parser
	 * no longer recognizes how to read the string.
	 * 
	 * @param e
	 *            The XPath expression
	 * @return A list of nodes, corresponding to the result of the operation.
	 *         The method always returns a list, even when the expected result
	 *         is true or false. In such a case, the list contains only one
	 *         element, a node of type TRUE or FALSE (not to be confused with a
	 *         <em>text</em> node whose text is "TRUE" or "FALSE"). Hence an
	 *         empty list is just an empty list of nodes, it should not be
	 *         confused with FALSE.
	 */
	public List<XMLDocument> evaluateXPath(String e) {
		Pattern pattern;
		Matcher matcher;
		XMLDocument xd_left, xd_right, xd_false, xd_true;
		String path_left = "", path_right = "";
		 List<XMLDocument> out = new Vector<XMLDocument>();
		List<XMLDocument> left, right;

		// Creates element false
		xd_false = new XMLDocument();
		xd_false.m_type = NodeType.FALSE;

		// Creates element true
		xd_true = new XMLDocument();
		xd_true.m_type = NodeType.TRUE;

		// Get left path
		pattern = Pattern.compile("(^[^\\s=]+)={0,1}", Pattern.DOTALL);
		matcher = pattern.matcher(e);
		if (!matcher.find()) {
			// Parse error: return false
			out.add(xd_false);
			return out;
		}
		path_left = matcher.group(1);
		e = e.substring(matcher.end()).trim();
		left = getPath(path_left);
		if (e.length() == 0) {
			// Nothing else: return subtree
			return left;
		}
		// Check if equality
		if (e.length() > 0 && !e.startsWith("=")) {
			// Parse error: return false
			out.add(xd_false);
			return out;
		}
		// Remove = sign
		e = e.substring(1).trim();
		// Check if constant
		if (e.startsWith("\"")) {
			pattern = Pattern.compile("\"([^=]*)\"", Pattern.DOTALL);
			matcher = pattern.matcher(e);
			if (!matcher.find()) {
				// Parse error: return false
				out.add(xd_false);
				return out;
			}
			path_right = matcher.group(1);
			if (left.size() != 1) {
				// LHS is a set of nodes and RHS is constant
				out.add(xd_false);
				return out;
			}
			if (left.get(0).m_type != NodeType.TEXT) {
				// LHS is not text
				out.add(xd_false);
				return out;
			}
			if ((m_caseSensitive == true && path_right.compareTo(left.get(0).m_elementName) != 0) || (m_caseSensitive == false && path_right.compareToIgnoreCase(left.get(0).m_elementName) != 0)) {
				// LHS and RHS text, but not same text
				out.add(xd_false);
				return out;
			}
			out.add(xd_true);
			return out;
		} else {
			// RHS is a path
			path_right = e;
			right = getPath(path_right);
			if (left.size() != 1 || right.size() != 1) {
				// One of the sides is not a single node
				out.add(xd_false);
				return out;
			}
			xd_left = left.get(0);
			xd_right = right.get(0);
			if (xd_left.m_type != NodeType.TEXT
					|| xd_right.m_type != NodeType.TEXT) {
				// One of the sides is not a text node
				out.add(xd_false);
				return out;
			}
			if ((m_caseSensitive == true && xd_left.m_elementName.compareTo(xd_right.m_elementName) != 0) || (m_caseSensitive == false && xd_left.m_elementName.compareToIgnoreCase(xd_right.m_elementName) != 0)) {
			  // LHS and RHS have different texts
			  out.add(xd_false);
			  return out;
			}
			out.add(xd_true);
			return out;
		}
	}

	protected List<XMLDocument> getPath(String path) {
		 List<XMLDocument> out = new Vector<XMLDocument>();
		if (path.length() == 0 || path.compareTo("/") == 0) {
			// End of path, return what's below
			return m_children;
		}
		 Pattern pattern = Pattern.compile("/([^/\\s]+)", Pattern.DOTALL);
		 Matcher matcher = pattern.matcher(path);
		if (!matcher.find()) {
			return out;
		}
		 String elementName = matcher.group(1);
		 String subPath = path.substring(matcher.end());
		 Iterator<XMLDocument> it = m_children.iterator();
		while (it.hasNext()) {
			 XMLDocument xd = it.next();
			if ((m_caseSensitive == true && elementName.compareTo(xd.m_elementName) == 0) || (m_caseSensitive == false && elementName.compareToIgnoreCase(xd.m_elementName) == 0)) {
				out.addAll(xd.getPath(subPath));
			}
		}
		return out;
	}

	public String getText() {
		if (m_type != NodeType.TEXT) {
			return "";
		}
		return m_elementName;
	}

	@Override
	public String toString() {
		return toString("");
	}

	protected String toString(String indent) {
		String out = "";
		 String subindent = (indent == null || m_type == NodeType.ROOT ? ""
				: indent + "  ");
		if (m_type == NodeType.TEXT) {
			return indent + m_elementName + "\n";
		} else if (m_type == NodeType.TRUE) {
			return indent + "TRUE\n";
		} else if (m_type == NodeType.FALSE) {
			return indent + "FALSE\n";
		} else if (m_type == NodeType.NODE) {
			out = indent + "<" + m_elementName;
			if (m_children == null || m_children.size() == 0) {
				return out + "/>\n";
			} else {
				out += ">\n";
			}
		}
		if (m_children != null && m_children.size() > 0) {
			 Iterator<XMLDocument> it = m_children.iterator();
			while (it.hasNext()) {
				 XMLDocument xd = it.next();
				out += xd.toString(subindent);
			}
		}
		if (m_type == NodeType.NODE) {
			out += indent + "</" + m_elementName + ">";
		}
		out += "\n";
		return out;
	}
	
	public NodeType getType()
	{
		return m_type;
	}
}
