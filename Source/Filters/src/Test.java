/**
	Filter Logic Library
	Copyright (C) 2011 Sylvain Hallé
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import ca.uqac.dim.filterlogic.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Test.
 */
public class Test
{

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		/*TraceMonitor m;
		Filter f = new Filter();
		ConstantMonitor p = new ConstantMonitor();
		p.setCondition("p");
		NegationMonitor nm = new NegationMonitor(p);
		f.setCondition(nm);
		TraceMonitor tm = new FalseMonitor();
		ValueSink sink = new ValueSink();
		f.setOutput(tm);
		Message msg;
		msg = new Message("p");
		f.putMessage(msg);
		System.out.println(tm.evaluate());
		//System.out.println(sink);
		msg = new Message("q");
		f.putMessage(msg);
		System.out.println(tm.evaluate());
		//System.out.println(sink);
		msg = new Message("p");
		f.putMessage(msg);
		System.out.println(tm.evaluate());
		//System.out.println(sink);
		 * 
		 */
		try
		{
			Filter m = FilterParser.parseFilterFromString("1000 / ((!p) | ((1/q) : 1))");
		}
		catch (FilterParser.ParseException e)
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		Filter un_sur_q = new Filter();
		un_sur_q.setCondition(new ConstantMonitor("q"));
		TraceMonitor un_sur_q_dedans = new TrueMonitor();
		un_sur_q_dedans.setFilter(un_sur_q);
		un_sur_q_dedans.toString();
		TraceMonitor p = new NegationMonitor(new ConstantMonitor("p"));
		OrMonitor gros_et = new OrMonitor();
		gros_et.setLeft(p);
		gros_et.setRight(un_sur_q_dedans);
		gros_et.toString();
		Filter filtre_principal = new Filter();
		filtre_principal.setCondition(gros_et);
		filtre_principal.setTopNumber(1000);
		ValueSink sink = new ValueSink();
		filtre_principal.setOutput(sink);
		AtomicMessage msg;
		msg = new AtomicMessage("p");
		filtre_principal.putMessage(msg);
		System.out.println(sink);
		msg = new AtomicMessage("p");
		filtre_principal.putMessage(msg);
		System.out.println(sink);
		System.out.println(filtre_principal);
		//false_final.putMessage(msg);
		//System.out.println(filtre_principal);
		//System.out.println(false_final.evaluate());
		msg = new AtomicMessage("q");
		filtre_principal.putMessage(msg);
		System.out.println(sink);
		//false_final.putMessage(msg);
		//System.out.println(filtre_principal);
		//System.out.println(false_final.evaluate());
	}

}
