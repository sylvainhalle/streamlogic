import ca.uqac.dim.filterlogic.*;

public class Test2
{
	public static void main(String[] args)
	{
		TraceMonitor m = null;
		try
		{
			m = FilterParser.parseMonitorFromString("(1 / ((!(p = \"3\")) | ((1/(q = \"3\")) : 1))) : 0");
		}
		catch (FilterParser.ParseException e)
		{
			System.err.println(e.getMessage());
		}
	}
}
